{ expect } = require 'chai'

id = (a) =>
  a

compose = (f, g) =>
  (a) =>
    g f a

add5 = (n) =>
  n + 5


object =
  10

expect (compose id, add5) object
  .to.equal (compose add5, id) object
  .and.to.equal add5 object
