import reveal from "reveal.js"
import mermaid from "mermaid"

reveal.initialize
  hash: yes
  fragmentInURl: yes

mermaid.initialize
  startOnLoad: yes

